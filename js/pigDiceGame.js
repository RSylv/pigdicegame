// Declaration des variables :
let score1;
let scoreTemp1;
let score2;
let scoreTemp2;
let playerTurn;
let tour;
let manche1;
let manche2;
let game = false;
let soundVolume = true;

// Dom dés fonctions
// DOM DE 1
let domDice = document.querySelector('.block');
let hg = document.querySelector('.hg');
let hd = document.querySelector('.hd');
let cg = document.querySelector('.cg');
let cc = document.querySelector('.cc');
let cd = document.querySelector('.cd');
let bg = document.querySelector('.bg');
let bd = document.querySelector('.bd');

// DOM DE 2
let domDice2 = document.querySelectorAll('.block2');
let hg2 = document.querySelector('.hg2');
let hd2 = document.querySelector('.hd2');
let cg2 = document.querySelector('.cg2');
let cc2 = document.querySelector('.cc2');
let cd2 = document.querySelector('.cd2');
let bg2 = document.querySelector('.bg2');
let bd2 = document.querySelector('.bd2');

// ALERT Div 
let title = document.getElementById('endTitle');
let manchePl1 = document.getElementById('manchePl1');
let manchePl2 = document.getElementById('manchePl2');
let EndBox = document.getElementById('endAlert');
let VictoryBox = document.getElementById('victoryAlert');
let Victory = document.getElementById('victoryTitle');

// SCORE MANCHE Box 
let mancheBox1 = document.getElementById('box1P1');
let mancheBox12 = document.getElementById('box2P1');
let mancheBox2 = document.getElementById('box1P2');
let mancheBox22 = document.getElementById('box2P2');

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                          Declaration des fonctions :
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////   GAME FLOW FUNCTION
function newGame() {
    score1 = 0;
    score2 = 0;
    scoreTemp1 = 0;
    scoreTemp2 = 0;
    playerTurn = true;
    activePlayer = 0;
    tour = 0;
    manche1 = 0;
    manche2 = 0;
    game = true;
    resetDice();

    document.getElementById('game').className = "gamein";
    document.getElementById('newGame').className = "button1";
    // document.getElementById("dice1").innerHTML = '<img class="imgDice" src="images/dice-1.png">';
    // document.getElementById("dice2").innerHTML = '<img class="imgDice" src="images/dice-1.png">';
    document.getElementById("Pl1").style.backgroundImage = "none";
    document.getElementById("Pl2").style.backgroundImage = "none";
    document.getElementById("score1").textContent = "";
    document.getElementById("score2").textContent = "";
    document.getElementById("curScore1").textContent = "";
    document.getElementById("curScore2").textContent = "";
    document.getElementById("on1").style.color = "grey";
    document.getElementById("on2").style.color = "grey";
    mancheBox1.style.backgroundColor = 'white';
    mancheBox12.style.backgroundColor = 'white';
    mancheBox2.style.backgroundColor = 'white';
    mancheBox22.style.backgroundColor = 'white';
}
function endGame() {
    audioVictoire();
    VictoryBox.className = "endBox";
    Victory.innerHTML = 'Victory !!' + '<br>' + 'Player ' + (activePlayer + 1);
    manchePl1.innerHTML = manche1;
    manchePl2.innerHTML = manche2;
    game = false;
}
function restart() {
    audioStart();
    VictoryBox.className = "endBoxOut";
    document.getElementById('game').className = "gameout";
    document.getElementById('newGame').className = "animestart";
}
function newManche() {
    score1 = 0;
    score2 = 0;
    scoreTemp1 = 0;
    scoreTemp2 = 0;
    playerTurn = true;
    activePlayer = 0;
    resetDice();

    EndBox.className = "endBoxOut";

    document.getElementById("Pl1").style.backgroundImage = "none";
    document.getElementById("Pl2").style.backgroundImage = "none";
    document.getElementById("score1").textContent = "";
    document.getElementById("score2").textContent = "";
    document.getElementById("curScore1").textContent = "";
    document.getElementById("curScore2").textContent = "";
    document.getElementById("on1").style.color = "grey";
    document.getElementById("on2").style.color = "grey";
}
function Manche() {
    if (100 <= score1 || 100 <= score2) {
        tour++;
        titleText = ('Manche : ' + tour);

        if (0 == activePlayer) {
            manche1++;
            if (2 <= manche1) {
                mancheBox12.style.backgroundColor = 'red';
                endGame();
            }
            else {
                audioManche();
                EndBox.className = "endBox";
                title.innerHTML = titleText;
                manchePl1.innerHTML = manche1;
                manchePl2.innerHTML = manche2;
                mancheBox1.style.backgroundColor = 'red';
            }
        }
        if (1 == activePlayer) {
            manche2++;
            if (2 <= manche2) {
                mancheBox22.style.backgroundColor = 'red';
                endGame();
            }
            else {
                audioManche();
                EndBox.className = "endBox";
                title.innerHTML = titleText;
                manchePl1.innerHTML = manche1;
                manchePl2.innerHTML = manche2;
                mancheBox2.style.backgroundColor = 'red';
            }
        }
    }
}
function resetDice() {
    hg.style.backgroundColor = 'white';
    hg.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.050)';
    hd.style.backgroundColor = 'white';
    hd.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.050)';
    cg.style.backgroundColor = 'white';
    cg.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.050)';
    cc.style.backgroundColor = 'white';
    cc.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.050)';
    cd.style.backgroundColor = 'white';
    cd.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.050)';
    bg.style.backgroundColor = 'white';
    bg.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.050)';
    bd.style.backgroundColor = 'white';
    bd.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.050)';

    hg2.style.backgroundColor = 'white';
    hg2.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.050)';
    hd2.style.backgroundColor = 'white';
    hd2.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.050)';
    cg2.style.backgroundColor = 'white';
    cg2.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.050)';
    cc2.style.backgroundColor = 'white';
    cc2.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.050)';
    cd2.style.backgroundColor = 'white';
    cd2.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.050)';
    bg2.style.backgroundColor = 'white';
    bg2.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.050)';
    bd2.style.backgroundColor = 'white';
    bd2.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.050)';
}


////////////////////////////////////////   AUDIO DOM FUNCTION
let startGame = document.getElementById("fanfare");
let bip = document.getElementById("bip");
let dice = document.getElementById("dice");
let applause = document.getElementById("applause");
let orchestral = document.getElementById("orchestral");
// DEFINITION DES VOLUMES
startGame.volume = 0.8;
dice.volume = 0.7;
bip.volume = 0.1;
applause.volume = 0.8;
orchestral.volume = 1;

function audioStart() {
    startGame.load();
    startGame.play();
}
function audioDice() {
    dice.load();
    dice.play();
}
function audio() {
    bip.load();
    bip.play();
}
function audioManche() {
    applause.load();
    applause.play();
}
function audioVictoire() {
    orchestral.load();
    orchestral.play();
}
// MUTE SOUND
function mute() {
    if (soundVolume) {
        startGame.volume = 0;
        dice.volume = 0;
        bip.volume = 0;
        applause.volume = 0;
        orchestral.volume = 0;
        document.getElementById("muteSoundText").innerHTML = '<img class="soundVolume" src="img/volume-mute-solid.png">';
        soundVolume = false;
    }
    else {
        startGame.volume = 0.8;
        dice.volume = 0.5;
        bip.volume = 0.1;
        applause.volume = 0.7;
        orchestral.volume = 0.8;
        document.getElementById("muteSoundText").innerHTML = '<img class="soundVolume" src="img/volume-up-solid.png">';
        soundVolume = true;
    }
}
// SON AU DEMMARRAGE
window.onload = function () { audioStart(); }


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                          Deroulement du jeu :
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function roll() {

    audioDice();
    resetDice();

    let dice1 = Math.floor(Math.random() * 6) + 1;
    let dice2 = Math.floor(Math.random() * 6) + 1;
    total = (dice1 + dice2);
    // document.getElementById("dice1").innerHTML = '<img class="imgDice" src="images/dice-' + dice1 + '.png">';
    // document.getElementById("dice2").innerHTML = '<img class="imgDice" src="images/dice-' + dice2 + '.png">';

    if (game) {


        // AFFICHAGE DE DICE2 ET DICE2
        if (1 == dice1) {
            cc.style.backgroundColor = 'red';
            cc.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
        }
        if (1 == dice2) {
            cc2.style.backgroundColor = 'red';
            cc2.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
        }
        if (2 == dice1) {
            hg.style.backgroundColor = 'red';
            hg.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            bd.style.backgroundColor = 'red';
            bd.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
        }
        if (2 == dice2) {
            hg2.style.backgroundColor = 'red';
            hg2.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            bd2.style.backgroundColor = 'red';
            bd2.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
        }
        if (3 == dice1) {
            hg.style.backgroundColor = 'red';
            hg.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            cc.style.backgroundColor = 'red';
            cc.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            bd.style.backgroundColor = 'red';
            bd.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
        }
        if (3 == dice2) {
            hg2.style.backgroundColor = 'red';
            hg2.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            cc2.style.backgroundColor = 'red';
            cc2.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            bd2.style.backgroundColor = 'red';
            bd2.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
        }
        if (4 == dice1) {
            hg.style.backgroundColor = 'red';
            hg.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            hd.style.backgroundColor = 'red';
            hd.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            bg.style.backgroundColor = 'red';
            bg.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            bd.style.backgroundColor = 'red';
            bd.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
        }
        if (4 == dice2) {
            hg2.style.backgroundColor = 'red';
            hg2.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            hd2.style.backgroundColor = 'red';
            hd2.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            bg2.style.backgroundColor = 'red';
            bg2.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            bd2.style.backgroundColor = 'red';
            bd2.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
        }
        if (5 == dice1) {
            hg.style.backgroundColor = 'red';
            hg.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            hd.style.backgroundColor = 'red';
            hd.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            cc.style.backgroundColor = 'red';
            cc.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            bg.style.backgroundColor = 'red';
            bg.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            bd.style.backgroundColor = 'red';
            bd.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
        }
        if (5 == dice2) {
            hg2.style.backgroundColor = 'red';
            hg2.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            hd2.style.backgroundColor = 'red';
            hd2.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            cc2.style.backgroundColor = 'red';
            cc2.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            bg2.style.backgroundColor = 'red';
            bg2.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            bd2.style.backgroundColor = 'red';
            bd2.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
        }
        if (6 == dice1) {
            hg.style.backgroundColor = 'red';
            hg.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            hd.style.backgroundColor = 'red';
            hd.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            cd.style.backgroundColor = 'red';
            cd.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            cg.style.backgroundColor = 'red';
            cg.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            bg.style.backgroundColor = 'red';
            bg.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            bd.style.backgroundColor = 'red';
            bd.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
        }
        if (6 == dice2) {
            hg2.style.backgroundColor = 'red';
            hg2.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            hd2.style.backgroundColor = 'red';
            hd2.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            cd2.style.backgroundColor = 'red';
            cd2.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            cg2.style.backgroundColor = 'red';
            cg2.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            bg2.style.backgroundColor = 'red';
            bg2.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
            bd2.style.backgroundColor = 'red';
            bd2.style.boxShadow = 'inset 3px 1px 10px rgba(0, 0, 0, 0.527)';
        }


        // GAME 

        if (playerTurn) {
            scoreTemp1 += total;

            document.getElementById("curScore1").textContent = scoreTemp1;
            document.getElementById("Pl1").style.backgroundImage = "url(img/paperLine.jpg)";
            document.getElementById("Pl2").style.backgroundImage = "none";
            document.getElementById("on1").style.color = "red";

            if (1 == dice1 && 1 == dice2) {
                audio();
                scoreTemp1 = 0;
                activePlayer = 1;
                playerTurn = false;

                document.getElementById("curScore1").textContent = "";
                document.getElementById("Pl2").style.backgroundImage = "url(img/paperLine.jpg)";
                document.getElementById("Pl1").style.backgroundImage = "none";
                document.getElementById("on1").style.color = "grey";
                document.getElementById("on2").style.color = "red";
            }
        }
        else {
            scoreTemp2 += total;
            document.getElementById("curScore2").textContent = scoreTemp2;
            if (1 == dice1 && 1 == dice2) {
                audio();
                scoreTemp2 = 0;
                activePlayer = 0;
                playerTurn = true;

                document.getElementById("curScore2").textContent = "";
                document.getElementById("Pl1").style.backgroundImage = "url(img/paperLine.jpg)";
                document.getElementById("Pl2").style.backgroundImage = "none";
                document.getElementById("on1").style.color = "red";
                document.getElementById("on2").style.color = "grey";
            }
        }

        hold = function () {
            if (playerTurn) {
                score1 += scoreTemp1;
                document.getElementById("score1").textContent = score1;
                Manche();
                scoreTemp1 = 0;
                activePlayer = 1;
                playerTurn = false;
                audio();

                document.getElementById("curScore1").textContent = "";
                document.getElementById("on1").style.color = "grey";
                document.getElementById("on2").style.color = "red";
                document.getElementById("Pl1").style.backgroundImage = "none";
                document.getElementById("Pl2").style.backgroundImage = "url(img/paperLine.jpg)";
            }
            else {
                score2 += scoreTemp2;
                document.getElementById("score2").textContent = score2;
                Manche();
                scoreTemp2 = 0;
                activePlayer = 0;
                playerTurn = true;
                audio();

                document.getElementById("curScore2").textContent = "";
                document.getElementById("on1").style.color = "red";
                document.getElementById("on2").style.color = "grey";
                document.getElementById("Pl1").style.backgroundImage = "url(img/paperLine.jpg)";
                document.getElementById("Pl2").style.backgroundImage = "none";
            }
        }
    }
}
